def func():
    print("FUN() in ONE.PY FILE")

print("GLOBAL STATEMENT")

if __name__ == '__main__':   # __name__ is built in variable name in python
    print("ONE.PY is running directly")

else:
    print("one.py has been imported")    